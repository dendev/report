import Chart from 'chart.js/auto';

var ctx = document.getElementById('report-statistic-chart').getContext('2d');
var datas = document.getElementById('report-statistic-datas').value;

datas = JSON.parse(datas);
var report_statistic_chart = new Chart(ctx, datas);

