<?php
?>
<a href="javascript:void(0)" onclick="runEntry(this)"
   data-route="{{route('report.statistic', ['report_id' => $entry->id])}}"
   data-button-type="statistic"
   class="btn btn-sm btn-link">
    <i class="la la-chart-line"></i> {{trans('dendev.report::report.operation_statistic_action')}}
</a>

<script>
    if (typeof runEntry != 'function') {
        var fct = document.querySelector("[data-button-type=run]")
        document.removeEventListener('click', fct);
        //.unbind('click');

        function runEntry(button) {
            // ask for confirmation before deleting an item
            // e.preventDefault();
            var button = $(button);
            var route = button.attr('data-route');
            var available = parseInt(button.attr('data-available'));
            var row = $("#crudTable a[data-route='"+route+"']").closest('tr');

            window.location.href = route;
        }
    }
    // make it so that the function above is run after each DataTable draw event
    // crud.addFunctionToDataTablesDrawEventQueue('deleteEntry');
</script>
