@extends(backpack_view('blank'))

@php
    $defaultBreadcrumbs = [
      trans('backpack::crud.admin') => url(config('backpack.base.route_prefix'), 'dashboard'),
      $crud->entity_name_plural => url($crud->route),
      trans('backpack::crud.preview') => false,
    ];

    // if breadcrumbs aren't defined in the CrudController, use the default breadcrumbs
    $breadcrumbs = $breadcrumbs ?? $defaultBreadcrumbs;
@endphp

@section('header')
    <section class="container-fluid d-print-none">
        <a href="javascript: window.print();" class="btn float-right"><i class="la la-print"></i></a>
        <h2>
            <span class="text-capitalize">{{trans('dendev.report::report.page_statistic_title')}}</span>
            <small>{{$crud->entry->title}}.</small>
            @if ($crud->hasAccess('list'))
                <small class=""><a href="{{ url($crud->route) }}" class="font-sm"><i class="la la-angle-double-left"></i> {{ trans('backpack::crud.back_to_all') }} <span>{{ $crud->entity_name_plural }}</span></a></small>
            @endif
        </h2>
    </section>
@endsection

@section('content')
    <input type="hidden" value="{{$chart_json}}" id="report-statistic-datas" name="report_statistic_data">
    <div class="row">
        <div class="col-md-12">
            <!-- Default box -->
            <div class="">
                @if ($crud->model->translationEnabled())
                    <div class="row">
                        <div class="col-md-12 mb-2">
                            <!-- Change translation button group -->
                            <div class="btn-group float-right">
                                <button type="button" class="btn btn-sm btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    {{trans('backpack::crud.language')}}: {{ $crud->model->getAvailableLocales()[request()->input('locale')?request()->input('locale'):App::getLocale()] }} &nbsp; <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu">
                                    @foreach ($crud->model->getAvailableLocales() as $key => $locale)
                                        <a class="dropdown-item" href="{{ url($crud->route.'/'.$entry->getKey().'/show') }}?locale={{ $key }}">{{ $locale }}</a>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                @endif

                <!-- display card -->
                <div  class="statistic-display-card">
                    <input type="hidden" name="statistic-display-submit-url" id="statistic-display-submit-url" value="{{route('report.statistic', ['report_id' => $crud->entry->id])}}">
                    <div class="form-group col-md-12">
                        <label class="form-input " for="select_statistic_display">{{ ucfirst(trans('dendev.report::report.field_statistic_display')) }}</label>
                        <select class="form-control select2_from_array" name="select_statistic_display" style="width: 100%" data-init-function="bpFieldInitSelect2FromArrayElement" id="select_statistic_display" onclick="dendev_report_statistic_select_statistic_display()">
                            @foreach($statistic_displays as $key => $value )
                                <option value="{{$key}}" @if( $key == $statistic_display_current ) selected @endif>{{$value}}</option>
                            @endforeach
                        </select>
                    </div>
                </form>

                <!-- statistic -->
                <div class="statistic-card">
                    <canvas id="report-statistic-chart"></canvas>
                    <p>{{$crud->entry->description}}</p>
                </div>

                <!-- debug -->
                @if( env('APP_DEBUG'))
                    <div class="statistic-debug-card ">
                        <p>
                            <button class="btn btn-primary statistic-debug-card_btn" type="button" data-toggle="collapse" data-target="#collapseDebug" aria-expanded="false" aria-controls="collapseDebug">
                                Debug
                            </button>
                        </p>
                        <div class="collapse statistic-debug-card_collapse" id="collapseDebug">
                            <div class="card card-body text-left">
                                @php
                                    $chart = json_decode($chart_json, true);
                                    dump( $chart);
                                @endphp
                            </div>
                        </div>
                    </div>
                @endif
                <!-- /.box-body -->
            </div><!-- /.box -->

        </div>
    </div>
@endsection

<script>
    function dendev_report_statistic_select_statistic_display()
    {
        var submit_url = document.getElementById('statistic-display-submit-url').value;
        var select_value = document.getElementById('select_statistic_display').value;
        var url = `${submit_url}/${select_value}`;
        location.href = url;
    }
</script>

@section('after_styles')
    <link rel="stylesheet" href="{{ asset('packages/backpack/crud/css/crud.css').'?v='.config('backpack.base.cachebusting_string') }}">
    <link rel="stylesheet" href="{{ asset('packages/backpack/crud/css/show.css').'?v='.config('backpack.base.cachebusting_string') }}">
    <link rel="stylesheet" href="{{ asset('packages/dendev/report/css/statistic.css').'?v='.config('backpack.base.cachebusting_string') }}">
@endsection

@section('after_scripts')
    <script src="{{ asset('packages/backpack/crud/js/crud.js').'?v='.config('backpack.base.cachebusting_string') }}"></script>
    <script src="{{ asset('packages/backpack/crud/js/show.js').'?v='.config('backpack.base.cachebusting_string') }}"></script>
    <script src="{{ asset('packages/dendev/report/js/chart.js').'?v='.config('backpack.base.cachebusting_string') }}"></script>
@endsection
