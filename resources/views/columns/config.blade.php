@php
$config = ( count( $entry->{$column['name']} ) > 0 ) ? $entry->{$column['name']} : false;
@endphp

@if( $config )
    <span>
    <table class="table table-bordered table-condensed table-striped m-b-0">
        <thead>
        <tr>
            <th>{{trans('dendev.report::report.field_config_responsive')}}</th>
            <th>{{trans('dendev.report::report.field_config_legend_position')}}</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>
                @if( $config['responsive'] )
                    {{trans('dendev.report::report.general_boolean_true')}}
                @else
                    {{trans('dendev.report::report.general_borlean_false')}}
                @endif
            </td>
            <td>
                {{$config['legend']['position']}}
            </td>
        </tr>
        </tbody>
    </table>
    </span>
@else

@endif


