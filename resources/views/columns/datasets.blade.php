@php
$datasets = ( count( $entry->{$column['name']} ) > 0 ) ? $entry->{$column['name']} : false;
@endphp

@if( $datasets )
    <span>
    <table class="table table-bordered table-condensed table-striped m-b-0">
        <thead>
        <tr>
            <th>{{trans('dendev.report::report.field_dataset_label')}}</th>
            <th>{{trans('dendev.report::report.field_dataset_getter')}}</th>
            <th>{{trans('dendev.report::report.field_dataset_data')}}</th>
            <th>{{trans('dendev.report::report.field_dataset_border_color')}}</th>
            <th>{{trans('dendev.report::report.field_dataset_background_color')}}</th>
        </tr>
        </thead>
        <tbody>
        @foreach($datasets as $dataset)
        <tr>
            <td>
                {{$dataset['label']}}
            </td>
            <td>
                {{$dataset['getter']}}
            </td>
            <td>
                @if(array_key_exists('data', $dataset))
                    <div style="overflow: auto; width: 100%">
                        {{implode(',', $dataset['data'])}}
                    </div>
                @endif
            </td>
            <td>
                {{$dataset['border_color']}}
            </td>
            <td>
                {{$dataset['background_color']}}
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>
    <p style="margin-top: .25rem;margin-bottom: .25rem;color: #73818f;font-size: 0.9em;">{{trans('dendev.report::report.field_datasets_warning')}}</p>
    </span>
@else

@endif


