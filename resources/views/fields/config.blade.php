<?php
$checkbox_config_responsive = old('checkbox_config_responsive') ? old('checkbox_config_responsive') : (isset($field['checkbox_config_responsive_value']) ? $field['checkbox_config_responsive_value'] : (isset($field['checkbox_config_responsive_default']) ? $field['checkbox_config_responsive_default'] : true ));
$select_config_legend_position = old('select_config_legend_position') ? old('select_config_legend_position') : (isset($field['select_config_legend_position_value']) ? $field['select_config_legend_position_value'] : (isset($field['select_config_legend_position_default']) ? $field['select_config_legend_position_default'] : 'top' ));
?>
<!-- field_type_name -->
@include('crud::fields.inc.wrapper_start')
    <label>{!! $field['label'] !!}</label>
<div class="well repeatable-element" style="padding: 7px 0px 7px 11px">
    <!-- responsive -->
    <div class="form-group col-sm-6">
        <div class="checkbox">
            <label class="form-input form-check-config-responsive" for="checkbox_config_responsive">{{ ucfirst(trans('dendev.report::report.field_config_responsive')) }} : </label>
            <input type="hidden" name="checkbox_config_responsive" value="{{$checkbox_config_responsive}}">
            <input type="checkbox" data-init-function="bpFieldInitCheckbox" id="checkbox_config_responsive"  @if( $checkbox_config_responsive) checked @endif>
        </div>
    </div>

    <!-- position -->
    <div class="form-group col-sm-6">
        <label class="form-input " for="select_config_legend_position">{{ ucfirst(trans('dendev.report::report.field_config_legend_position')) }}</label>
        <select class="form-control select2_from_array" name="select_config_legend_position" style="width: 100%" data-init-function="bpFieldInitSelect2FromArrayElement">
            <option value="top" @if( $select_config_legend_position === 'top' ) selected @endif>{{ ucfirst(trans('dendev.report::report.field_config_legend_position_top')) }}</option>
            <option value="bottom" @if( $select_config_legend_position === 'bottom' ) selected @endif>{{ ucfirst(trans('dendev.report::report.field_config_legend_position_bottom')) }}</option>
        </select>
    </div>
</div>

{{-- HINT --}}
@if (isset($field['hint']))
    <p class="help-block">{!! $field['hint'] !!}</p>
@endif
@include('crud::fields.inc.wrapper_end')

@if ($crud->fieldTypeNotLoaded($field))
    @php
        $crud->markFieldTypeAsLoaded($field);
    @endphp

    {{-- FIELD EXTRA CSS  --}}
    {{-- push things in the after_styles section --}}
    @push('crud_fields_styles')
        <!-- no styles -->
    @endpush

    {{-- FIELD EXTRA JS --}}
    {{-- push things in the after_scripts section --}}
    @push('crud_fields_scripts')
        <!-- no scripts -->
    @endpush
@endif
