<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Dendev\Report Translation Lines
    |--------------------------------------------------------------------------
    */

    // general
    'general_boolean_true' => 'true',
    'general_boolean_false' => 'false',

    // db fields
    'field_title'   => 'titre',
    'field_slug'   => 'slug',
    'field_description'   => 'description',
    'field_statistic_display'   => 'display mode',
    'field_labels'   => 'labels',
    'field_label'   => 'label',
    'field_label_add'   => 'add label',
    'field_datasets'   => 'datasets',
    'field_datasets_warning'   => 'Please check if your getter class name ( NameSpace\Class::method ) and method exist.',
    'field_dataset_label'   => 'label',
    'field_dataset_data'   => 'data',
    'field_dataset_getter'   => 'getter',
    'field_dataset_border_color'   => 'border color',
    'field_dataset_border_color_hint'   => 'empty for random. ; for multi choices',
    'field_dataset_background_color'   => 'background color',
    'field_dataset_background_color_hint'  => 'empty for random. ; for multi choices',
    'field_dataset_add'   => 'add dataset',
    'field_config'   => 'config',
    'field_config_responsive'   => 'responsive',
    'field_config_legend_position'   => 'position',
    'field_config_legend_position_top'   => 'top',
    'field_config_legend_position_bottom'   => 'bottom',

    // statistics
    'statistic_display_line' => 'line',
    'statistic_display_bar' => 'bar',
    'statistic_display_stacked_bar' => 'stacked bar',
    'statistic_display_bubble' => 'bubble',
    'statistic_display_doughnut' => 'doughnut',
    'statistic_display_pie' => 'pie',
    'statistic_display_polar' => 'polar',
    'statistic_display_radar' => 'radar',

    // operation
    'operation_run_action' => 'Run',
    'operation_run_success' => 'Snapshot took',
    'operation_run_no_datas' => 'No snapshot took. <br>Please check ou class and method getter exist.',
    'operation_statistic_action' => 'Stats',

    // page
    'page_statistic_title' => 'statistics',

    // validation
    'validation_required' => 'The :attribute field is required.',



];
