<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Dendev\Report Translation Lines
    |--------------------------------------------------------------------------
    */

    // general
    'general_boolean_true' => 'oui',
    'general_boolean_false' => 'non',

    // db fields
    'field_title'   => 'titre',
    'field_slug'   => 'slug',
    'field_description'   => 'description',
    'field_statistic_display'   => 'affichage',
    'field_labels'   => 'labels',
    'field_label'   => 'label',
    'field_label_add'   => 'ajout label',
    'field_datasets'   => 'datasets',
    'field_datasets_warning'   => 'Vérifiez que la classe et la methode du getter existe bien ( NameSpace\Class::method ).',
    'field_dataset_label'   => 'label',
    'field_dataset_data'   => 'data',
    'field_dataset_getter'   => 'getter',
    'field_dataset_border_color'   => 'couleur bordure',
    'field_dataset_border_color_hint'   => 'vide pour autogénérer. ; pour valeurs multiple',
    'field_dataset_background_color'   => 'couleur arrière plan',
    'field_dataset_background_color_hint'   => 'vide pour autogénérer. ; pour valeurs multiple',
    'field_dataset_add'   => 'ajout dataset',
    'field_config'   => 'config',
    'field_config_responsive'   => 'responsive',
    'field_config_legend_position'   => 'position',
    'field_config_legend_position_top'   => 'haut',
    'field_config_legend_position_bottom'   => 'bas',

    // statistics
    'statistic_display_line' => 'line',
    'statistic_display_bar' => 'bar',
    'statistic_display_stacked_bar' => 'stacked bar',
    'statistic_display_bubble' => 'bubble',
    'statistic_display_doughnut' => 'doughnut',
    'statistic_display_pie' => 'pie',
    'statistic_display_polar' => 'polar',
    'statistic_display_radar' => 'radar',

    // operation
    'operation_run_action' => 'Run',
    'operation_run_success' => 'Snapshot',
    'operation_run_no_datas' => 'Pas de snapshit. <br>Vérifier que le method et la classe du getter existent.',
    'operation_statistic_action' => 'Stats',

    // page
    'page_statistic_title' => 'statistiques',

    // validation
    'validation_required' => 'le champ :attribute est obligatoire.',
];
