<?php

/*
|--------------------------------------------------------------------------
| Dendev\Report Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are
| handled by the Dendev\Report package.
|
*/

/**
 * User Routes
 */

// Route::group([
//     'middleware'=> array_merge(
//     	(array) config('backpack.base.web_middleware', 'web'),
//     ),
// ], function() {
//     Route::get('something/action', \Dendev\Report\Http\Controllers\SomethingController::actionName());
// });


/**
 * Admin Routes
 */

use Dendev\Report\Http\Controllers\Admin\ReportCrudController;

Route::group([
    'prefix' => config('backpack.base.route_prefix', 'admin'),
    'middleware' => array_merge(
        (array) config('backpack.base.web_middleware', 'web'),
        (array) config('backpack.base.middleware_key', 'admin')
    ),
], function () {
    Route::crud('report', ReportCrudController::class);
});
