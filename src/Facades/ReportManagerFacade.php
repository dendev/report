<?php

namespace Dendev\Report\Facades;

use Illuminate\Support\Facades\Facade;

class ReportManagerFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'report_manager';
    }
}
