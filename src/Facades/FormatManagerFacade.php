<?php

namespace Dendev\Report\Facades;

use Illuminate\Support\Facades\Facade;

class FormatManagerFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'format_manager';
    }
}
