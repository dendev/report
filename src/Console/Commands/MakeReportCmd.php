<?php

namespace Dendev\Report\Console\Commands;

use Illuminate\Console\GeneratorCommand;

class MakeReportCmd extends GeneratorCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'report:make_report_cmd {name : name of the futur command}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'make a laravel command for run a report';

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub()
    {
        return __DIR__ . '/../../../stubs/ReportDemandCreatedByMonth.php';
    }

    /**
     * Execute the console command.
     *
     * @return bool|null
     */
    public function handle()
    {
        $cmd_name = $this->argument('name');
        $cmd_name = ucfirst($cmd_name);
        if( str_starts_with($cmd_name, 'Report') === false )
            $cmd_name = 'Report' . $cmd_name;

        $destination_path = $this->laravel['path']."/Console/Commands/$cmd_name.php";

        if ($this->files->exists($destination_path)) {
            $this->error('Command already exists!');

            return false;
        }

        $this->makeDirectory($destination_path);

        $this->files->put($destination_path, $this->buildClass($cmd_name));

        $this->info($this->laravel->getNamespace()."Console\Commands\\$cmd_name created successfully.");
    }

    /**
     * Build the class. Replace Leodel namespace with App one.
     *
     * @param string $name
     *
     * @return string
     */
    protected function buildClass($name)
    {
        $stub = $this->files->get($this->getStub());

        return $this->makeReplacements($stub, $name);
    }

    /**
     * Replace the namespace for the given stub.
     * Replace the User model, if it was moved to App\Models\User.
     *
     * @param string $stub
     * @param string $name
     *
     * @return string|string[]
     */
    protected function makeReplacements(&$stub, $name)
    {
        $name_snake = strtolower(preg_replace('/(?<!^)[A-Z]/', '_$0', $name));
        $name_snake = (str_replace('report_', '', $name_snake));

        $stub = str_replace('Dendev\Report\Console\Commands;', $this->laravel->getNamespace().'Console\Commands;', $stub);
        $stub = str_replace('ReportDemandCreatedByMonth', $name, $stub);
        $stub = str_replace('report_demand_created_by_months', $name_snake, $stub);

        if (! $this->files->exists($this->laravel['path']."/Console/Commands/$name.php")) {
            $stub = str_replace($this->laravel->getNamespace().$name, $this->laravel->getNamespace().'Console\Commands\\'.$name, $stub);
        }

        return $stub;
    }
}
