<?php

namespace Dendev\Report\Console\Commands;

use Illuminate\Console\Command;

class Install extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'report:install {--p|publish : publish config and lang files}';

    /**
     * The console command description.
     *
     *
     * @var string
     */
    protected $description = 'Install Report package';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->publish_config();
        $this->publish_lang();

        $this->migrate();

        $this->about_usage();
    }

    // Publish
    public function publish_config()
    {
        if( $this->option('publish') )
        {
            $this->call('vendor:publish', [
                '--provider' => 'Dendev\Report\AddonServiceProvider',
                '--tag' => 'config'
            ]);

            $this->info('[Report] config published in config/dendev/report/');

            $this->about_config();
        }
    }

    public function publish_lang()
    {
        if( $this->option('publish') === true)
        {
            $this->call('vendor:publish', [
                '--provider' => 'Dendev\Report\AddonServiceProvider',
                '--tag' => 'lang'
            ]);

            $this->info('[Report] lang published in resources/lang/vendor/dendev');

            $this->about_config();
        }
    }

    // Migrate
    public function migrate()
    {
        $this->call('migrate', []);

        $this->info('');
        $this->info('[Report] Migration: create reports table');
    }

    // About
    public function about_config()
    {
        /* // not needed yet
        $configs = [
            ['ignores', 'List of laravel command would be ignored by cronit'],
        ];
        $this->info('');
        $this->info("[Report] Infos" );

        foreach( $configs as $config )
        {
            $key = $config[0];
            $value = $config[1];

            $config_str = "{$key} : $value";
            $this->info($config_str);
        }
        */
        $this->info('No config needed');
    }

    public function about_lang()
    {
        $this->info('');
        $this->info("[Lang] Infos" );
        $this->info("just edit files in resources/lang/vendor/dendev");
    }

    public function about_usage()
    {
        $this->info('');
        $this->info("[Usage] Admin" );
        $this->info("add in link to report in sidebar_content.blade.php");
        $this->info("vim resources/views/vendor/backpack/base/inc/sidebar_content.blade.php");
        $this->info("<li class='nav-item'><a class='nav-link' href='{{ backpack_url('report') }}'><i class='nav-icon fas fa-chart-line'></i> Report</a></li>");


        $this->info('');
        $this->info("[Usage] Next ?" );
        $this->info("Create laravel commande to run each day/month/year a data report" );
        $this->info("php artisan report:make_report_cmd CmdName");
    }
}
