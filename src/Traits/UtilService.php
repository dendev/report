<?php


namespace Dendev\Report\Traits;


trait UtilService
{
    /**
     * Crée une instance du model si l'argument est un id
     *
     * @param mixed $id_or_model id ou model
     * @param string $classname nom de la classe à instancié
     * @return Model retourne un objet de type model
     */
    private function _instantiate_if_id($id_or_model, $classname)
    {
        if( is_object($id_or_model) && get_class($id_or_model) == $classname )
        {
            $model = $id_or_model;
        }
        else
        {
            $model = $classname::find($id_or_model);
            if (is_null($model))
            {
                \Log::error("[UtilService:_instantiate_if_id] USiii00: Echec instantiation du model '$classname'. L'id '$id_or_model' n'est pas bon", [
                    'id' => $id_or_model,
                    'classe' => $classname,
                ]);

            }
        }

        return $model;
    }

    private function _add_extra_filters($rqs, $extras)
    {
        if( ! is_array($extras) )
            throw new \Exception(__METHOD__ . '# Réclame un tableau en second argument');

        foreach ($extras as $extra)
        {
            $column = $extra['column'];
            $operator = $extra['operator'];
            $value = $extra['value'];

            $rqs = $rqs->where($column, $operator, $value);
        }

        return $rqs;
    }
}
