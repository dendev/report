<?php

namespace Dendev\Report\Services\Formats;

abstract class AFormat
{
    public function to_chart($report, $output)
    {
        $chart = [];

        // data
        $labels = $report->labels;
        $datasets = $report->datasets;
        $config = $report->config;

        if ($labels && $datasets)
        {
            //  //  labels
            $labels_formated = $this->_make_labels($labels);

            //  //  datasets
            $to_add = $this->_set_to_add($config);
            $nb_labels = count( $labels_formated);
            $datasets_formated = $this->_make_datasets($datasets, $to_add, $nb_labels);

            //  // data
            $data = $this->_make_data($labels_formated, $datasets_formated);

            //  // options
            $options = $this->_make_options($report->title, $config);

        }
        else
            {
            \Log::error("[Report::AFormat::to_chart] no valid labels or/and no valid datasets found in report", [
                'labels' => $labels,
                'datasets' => $datasets,
                'report' => $report
            ]);
        }

        // chart with type
        $chart = [
            'type' => $this->_type,
            'data' => $data,
            'options' => $options,
        ];

        // output
        if ($output === 'json')
        {
            $chart = json_encode($chart, true);
        }


        return $chart;
    }

    protected function _get_random_color()
    {
        $rcp = function () {
            return str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT);
        };
        return "#" . $rcp() . $rcp() . $rcp();

    }

    protected function _get_config_values($config)
    {
        $responsive = ( array_key_exists('responsive', $config) ) ? $config['responsive'] : true;
        $position = ( array_key_exists('legend', $config) && array_key_exists('position', $config['legend']) ) ? $config['legend']['position'] : 'top';
        $fill = ( array_key_exists('line', $config) && array_key_exists('fill', $config['line']) ) ? $config['line']['fill'] : false;
        $tension = ( array_key_exists('line', $config) && array_key_exists('tension', $config['line']) ) ? $config['line']['tension'] : 0.1;

        return [
            'responsive' => $responsive,
            'position' => $position,
            'fill' => $fill,
            'tension' => $tension,
        ];
    }

    protected function _make_labels($labels)
    {
        $labels_formated = [];
        foreach ($labels as $key => $label)
            $labels_formated[] = $label;

        return $labels_formated;
    }

    protected function _make_datasets($datasets, $to_add = [], $nb_labels = 0)
    {
        $datasets_formated = [];

        foreach ($datasets as $dataset)
        {
            // add general
            foreach( $to_add as $key => $value )
            {
                $dataset[$key] = $value;
            }

            // add specific
            $dataset = $this->_set_border_color($dataset, $nb_labels);
            $dataset = $this->_set_background_color($dataset, $nb_labels);

            // clean
            unset($dataset['getter']);
            unset($dataset['border_color']);
            unset($dataset['background_color']);

            // finish
            $datasets_formated[] = $dataset;
        }

        return $datasets_formated;
    }

    protected function _make_data($labels, $datasets)
    {
        $data = [
            'labels' => $labels,
            'datasets' => $datasets
        ];

        return $data;
    }

    protected function _make_options($title, $config)
    {
        $config_values = $this->_get_config_values($config);

        $options = [
            'responsive' => $config_values['responsive'],
            'plugins' => [
                'legend' => [
                    'position' => $config_values['position'],
                ],
                'title' => [
                    'display' => true,
                    'text' => $title
                ]
            ]
        ];

        if( array_key_exists('scales', $config_values))
            $options['scales'] = $config_values['scales'];

        return $options;
    }

    protected function _set_border_color($dataset, $nb)
    {
        $dataset['borderColor'] = $this->_define_color($dataset, 'border_color');
        return $dataset;
    }

    protected function _set_background_color($dataset, $nb)
    {

        $dataset['backgroundColor'] = $this->_define_color($dataset, 'background_color');
        return $dataset;
    }

    protected function _convert_color_hexa_to_rgb( $color, $opacity = false )
    {
        $default = 'rgb( 0, 0, 0 )';

        /**
         * Return default if no color provided
         */
        if( empty( $color ) ) {
            return $default;
        }

        /**
         * Sanitize $color if "#" is provided
         */
        if ( $color[0] == '#' ) {
            $color = substr( $color, 1 );
        }

        /**
         * Check if color has 6 or 3 characters and get values
         */
        if ( strlen($color) == 6 ) {
            $hex = array( $color[0] . $color[1], $color[2] . $color[3], $color[4] . $color[5] );
        }
        elseif ( strlen( $color ) == 3 )
        {
            $hex = array( $color[0] . $color[0], $color[1] . $color[1], $color[2] . $color[2] );
        }
        else
        {
            return $default;
        }

        /**
         * [$rgb description]
         * @var array
         */
        $rgb =  array_map( 'hexdec', $hex );

        /**
         * Check if opacity is set(rgba or rgb)
         */
        if( $opacity )
        {
            if( abs( $opacity ) > 1 )
                $opacity = 1.0;
            $output = 'rgba( ' . implode( "," ,$rgb ) . ',' . $opacity . ' )';
        }
        else
        {
            $output = 'rgb( ' . implode( "," , $rgb ) . ' )';
        }

        return $output;
    }

    protected function _define_colors($dataset, $type, $nb, $fixed_color = false)
    {
        $colors = [];

        for( $i = 0; $i < $nb; $i++)
        {
            $colors[] = $this->_define_color($dataset, $type, $i, $fixed_color);
        }

        return $colors;
    }

    protected function _define_color($dataset, $type, $key = 0, $fixed_color = false)
    {
        if( $fixed_color)
        {
            $color = $fixed_color;
        }
        else
        {
            $color_user = (array_key_exists($type, $dataset) && $dataset[$type]) ? $dataset[$type] : $this->_get_random_color();
            $colors_user = explode(',', $color_user);
            $color = array_key_exists($key, $colors_user) && $colors_user[$key] ? $colors_user[$key] : $this->_get_random_color();
        }

        $color = $this->_convert_color_hexa_to_rgb($color, 0.5);

        return $color;
    }



    protected function _set_to_add($config)
    {
        $to_add = [];
        return $to_add;
    }
}

