<?php

namespace Dendev\Report\Services\Formats;


/**
 * Class ReportManagerService
 * @package Dendev\Report
 */
class StackedBarFormat extends AFormat
{
    protected $_type = 'bar';

    protected function _get_config_values($config)
    {
        $values = parent::_get_config_values($config);
        $values['scales'] = [
            'x' => ['stacked' => true],
            'y' => ['stacked' => true],
        ];

        return $values;
    }

}

