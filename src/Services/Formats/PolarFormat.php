<?php

namespace Dendev\Report\Services\Formats;


class PolarFormat extends AFormat
{
    protected $_type = 'polarArea';

    protected function _set_border_color($dataset, $nb)
    {
        $dataset['borderColor'] = $this->_define_colors($dataset, 'border_color', $nb, $fixed_color='#F1F4F8');
        return $dataset;
    }

    protected function _set_background_color($dataset, $nb)
    {

        $dataset['backgroundColor'] = $this->_define_colors($dataset, 'background_color', $nb);
        return $dataset;
    }
}

