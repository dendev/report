<?php

namespace Dendev\Report\Services\Formats;


class LineFormat extends AFormat
{
    protected $_type = 'line';

    protected function _set_to_add($config)
    {
        $to_add = [];

        $config_values = $this->_get_config_values($config);
        $to_add['fill'] = $config_values['fill'];
        $to_add['tension'] = $config_values['tension'];

        return $to_add;
    }


}

