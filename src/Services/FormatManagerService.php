<?php

namespace Dendev\Report\Services;


use Dendev\Report\Services\Formats\BubbleFormat;
use Dendev\Report\Services\Formats\PieFormat;
use Dendev\Report\Services\Formats\PolarFormat;
use Dendev\Report\Services\Formats\RadarFormat;
use Dendev\Report\Services\Formats\StackedBarFormat;
use Dendev\Report\Traits\UtilService;
use Dendev\Report\Models\Report;
use Dendev\Report\Services\Formats\LineFormat;
use Dendev\Report\Services\Formats\BarFormat;
use Dendev\Report\Services\Formats\DoughnutFormat;

/**
 * Class ReportManagerService
 * @package Dendev\Report
 */
class FormatManagerService
{
    use UtilService;

    public function test_me()
    {
        return true;
    }

    public function get_available_statistic_displays()
    {
        return [
            'line' => ucfirst( trans('dendev.report::report.statistic_display_line') ),
            'bar' => ucfirst( trans('dendev.report::report.statistic_display_bar') ),
            'stacked_bar' => ucfirst( trans('dendev.report::report.statistic_display_stacked_bar') ),
            'bubble' => ucfirst( trans('dendev.report::report.statistic_display_bubble') ),
            'doughnut' => ucfirst( trans('dendev.report::report.statistic_display_doughnut') ),
            'pie' => ucfirst( trans('dendev.report::report.statistic_display_pie') ),
            'polar' => ucfirst( trans('dendev.report::report.statistic_display_polar') ),
            'radar' => ucfirst( trans('dendev.report::report.statistic_display_radar') ),
        ];
    }

    public function format_to_type($type, $id_or_model_report, $output = 'json')
    {
        $datas = [];
        $report = $this->_instantiate_if_id($id_or_model_report, Report::class);

        if( $report)
        {
            switch ($type)
            {
                case 'line':
                    $formatter = new LineFormat();
                    $datas = $formatter->to_chart($report, $output);
                    break;
                case 'bar':
                case 'vertical_bar':
                    $formatter = new BarFormat();
                    $datas = $formatter->to_chart($report, $output);
                    break;
                case 'stacked_bar':
                    $formatter = new StackedBarFormat();
                    $datas = $formatter->to_chart($report, $output);
                    break;
                case 'doughnut':
                    $formatter = new DoughnutFormat();
                    $datas = $formatter->to_chart($report, $output);
                    break;
                case 'bubble':
                    $formatter = new BubbleFormat();
                    $datas = $formatter->to_chart($report, $output);
                    break;
                case 'pie':
                    $formatter = new PieFormat();
                    $datas = $formatter->to_chart($report, $output);
                    break;
                case 'radar':
                    $formatter = new RadarFormat();
                    $datas = $formatter->to_chart($report, $output);
                    break;
                case 'polar':
                    $formatter = new PolarFormat();
                    $datas = $formatter->to_chart($report, $output);
                    break;
                default:
                    $formatter = new LineFormat();
                    $datas = $formatter->to_chart($report, $output);
            }
        }

        return $datas;
    }

}

