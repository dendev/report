<?php

namespace Dendev\Report\Services;


use Dendev\Report\Traits\UtilService;
use Dendev\Report\Models\Report;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Str;
use Symfony\Component\Console\Exception\CommandNotFoundException;
use Tests\Unit\Mocks\Getter;

/**
 * Class ReportManagerService
 * @package Dendev\Report
 */
class ReportManagerService
{
    use UtilService;

    public function create($title, $labels, $datasets, $config = false, $description = false, $statistic_display = 'line')
    {
        // create
        $report = new Report();

        // create or update
        $slug = Str::slug($title);
        $already_exist = Report::where('slug', $slug)->first();
        if( $already_exist)
        {
            $report = $already_exist;
        }

        // title
        $report->title = $title;

        // labels
        if( is_array($labels) )
        {
            $report->labels = $labels;
        }

        // datas
        if( is_array($datasets) )
        {
            $valids = [];
            foreach( $datasets as $dataset )
            {
                $is_valid = $this->_check_dataset($dataset);
                if( $is_valid )
                    $valids[] = $dataset;
            }

            if( count( $valids ) > 0 )
                $report->datasets = $valids;
            else
                \Log::error("[Report::ReportManagerService::create] no valid dataset provided",[
                    'datasets' => $datasets,
                    'valids' => $valids,
                    'nb_valids' => count( $valids)
                ]);
        }

        // config
        if( $config )
        {
            $report->config = $config;
        }
        else
        {
            $default = ['title' => ['display' => true] ]; // todo from config file ?
            $report->config = $default;
        }

        // description
        if( $description )
            $report->description = $description;

        // statistic display
        if( $statistic_display )
            $report->statistic_display = $statistic_display;

        $report->save();

        return $report;
    }

    public function add_dataset_to_report($dataset, $id_or_model_report)
    {
        $report = $this->_instantiate_if_id($id_or_model_report, Report::class);

        if( $report )
        {
            $is_valid = $this->_check_dataset($dataset);
            if( $is_valid )
            {
                $datasets = $report->datasets;
                $datasets[] = $dataset;
                $report->datasets = $datasets;
                $report->save();
            }
            else
            {
                \Log::error('[Report::ReportManager::_add_dataset_to_report] invalid dataset',[
                    'dataset' => $dataset
                ]);
            }
        }
        else
        {
            \Log::error('[Report::ReportManager::_add_dataset_to_report] no report found',[
                'report' => $report
            ]);
        }

        return $report;
    }

    /**
     * Add some data in specific dataset of a report
     *
     * @param $data array or value to add
     * @param $dataset_index
     * @param $id_or_model_report
     * @return \Dendev\Report\Traits\Model
     */
    public function add_data_to_dataset_of_report($data, $dataset_index, $id_or_model_report)
    {
        $report = $this->_instantiate_if_id($id_or_model_report, Report::class);

        if( ! is_array($data) )
            $data = [ $data];

        if( $report)
        {
            $data_from_dataset = \ReportManager::get_data_from_dataset_of_report($dataset_index, $report);

            // merge values
            $data_updated = array_merge($data_from_dataset, $data);
            // get dataset
            $dataset = \ReportManager::get_dataset_of_report($dataset_index, $id_or_model_report);
            // update dataset
            $dataset['data'] = $data_updated;
            // update datasets
            $datasets = $report->datasets;
            $datasets[$dataset_index] = $dataset;
            // update report
            $report->datasets = $datasets;
            $report->save();
        }

        return $report;
    }

    public function get_available_statistic_displays()
    {
        return \FormatManager::get_available_statistic_displays();
    }

    public function get_dataset_of_report($index, $id_or_model_report)
    {
        $dataset = [];

        $report = $this->_instantiate_if_id($id_or_model_report, Report::class);

        if( $report )
        {
            if( $report->datasets && count( $report->datasets ) > 0 )
            {
                $datasets = $report->datasets;
                if( array_key_exists($index, $datasets))
                {
                    $dataset = $datasets[$index];
                }
            }
        }

        return $dataset;
    }

    public function get_data_from_dataset_of_report($index, $id_or_model_report)
    {
        $data = [];

        $dataset = \ReportManager::get_dataset_of_report($index, $id_or_model_report);
        if( array_key_exists('data', $dataset) )
        {
            $data = $dataset['data'];
        }
        else
        {
            $data = [];
        }

        return $data;
    }

    public function get_getter_from_dataset_of_report($index, $id_or_model_report, $parsed = false)
    {
        $getter = false;
        $dataset = \ReportManager::get_dataset_of_report($index, $id_or_model_report);

        if( $dataset )
        {
            // get value
            if( array_key_exists('getter', $dataset) )
            {
                // format value
                $found = $dataset['getter'];
                $getter = $this->_check_getter($found);

                if( $getter && $parsed )
                    $getter = $this->_parse_getter($getter);
            }
            else
            {
                \Log::error('[Report::ReportManager::_check_dataset] no getter found for dataset',[
                    'dataset' => $dataset
                ]);
            }

        }

        return $getter;
    }

    public function run_getter_from_dataset_of_report($index, $id_or_model_report)
    {
        $data = false;

        $getter = $this->get_getter_from_dataset_of_report($index, $id_or_model_report, true);

        if( $getter)
        {
            $class_name = $getter['class_name'];
            $method_name = $getter['method_name'];

            $class = new $class_name;
            $data = $class->$method_name();
        }


        return $data;
    }

    public function reset_data_from_dataset_of_report($index, $id_or_model_report)
    {
        $ok = false;
        $report = $this->_instantiate_if_id($id_or_model_report, Report::class);

        if( $report )
        {
            $datasets = $report->datasets;
            $datasets[$index]['data'] = [];
            $report->datasets = $datasets;
            $report->save();
            $ok = true;
        }

        return $ok;
    }

    public function take_snapshot($id_or_model_report, $save = false, $format = 'raw')
    {
        $formated = [];
        $snaps = [];
        $report = $this->_instantiate_if_id($id_or_model_report, Report::class);

        if( $report )
        {
            $datasets = $report->datasets;
            foreach( $datasets as $index => $dataset )
            {
                $label = $dataset['label'];
                $data = $this->run_getter_from_dataset_of_report($index, $report);
                $took_at = now();

                if( $data)
                {
                    $snap = [
                        'label' => $label,
                        'data' => $data,
                        'took_at' => $took_at
                    ];
                    $snaps[] = $snap;
                }
            }

            // format
            if( count( $snaps) > 0 )
            {
                if( $format === 'string' || $format === 'text' )
                {
                    foreach( $snaps as $sn )
                    {
                        $formated[] = $sn['label'] . ' : ' . $sn['data'];
                    }
                }
                else
                {
                    $formated = $snaps;
                }

            }

            // save
            if( $save )
            {
                foreach( $snaps as $key => $sn )
                {
                    $data = $sn['data'];
                    $this->add_data_to_dataset_of_report($data, $key, $report);
                }
            }
        }

        return $formated;
    }

    /**
     * Test stupide pour vérifier que le service est utilisable
     * @return bool
     */
    public function test_me()
    {
        return true;
    }

    public function export_as_chart($type, $id_or_model_report, $output = 'json')
    {
        $datas = [];
        $report = $this->_instantiate_if_id($id_or_model_report, Report::class);

        if( $report)
        {
            $datas = \FormatManager::format_to_type($type, $report, $output);
        }

        return $datas;
    }

    // -
    private function _check_dataset($dataset)
    {
        $ok = false;

        if( is_array($dataset))
        {
            if( ( array_key_exists('label', $dataset) && $dataset['label'])
                // && ( array_key_exists('data', $dataset) && $dataset['data'])
                && ( array_key_exists('getter', $dataset) && $dataset['getter']) )
            {
                $ok = true;
            }
            else
            {
                \Log::error('[Report::ReportManager::_check_dataset] Bad dataset. Dataset must be an array with label, data, getter keys with value',[
                    'dataset' => $dataset
                ]);
            }
        }
        else
        {
            \Log::error('[Report::ReportManager::_check_dataset] Bad dataset. Dataset must be an array',[
                'dataset' => $dataset
            ]);
        }

        return $ok;
    }

    private function _check_getter($found)
    {
        $getter = false;
        // parse
        $getter_parsed = $this->_parse_getter($found);

        if( $getter_parsed)
        {
            // check exist
            $exist = method_exists($getter_parsed['class_name'], $getter_parsed['method_name']);
            if ($exist)
            {
                $getter = $found;
            }
            else
            {
                \Log::error('[Report::ReportManager::_check_dataset] getter methode not found', [
                    'class_name' => $getter_parsed['class_name'],
                    'method' => $getter_parsed['method_name']
                ]);
            }
        }

        return $getter;
    }

    private function _parse_getter($getter)
    {
        $getter_parsed = false;

        $tmp = explode('::', $getter);
        if( count( $tmp ) == 2 )
        {
            $class_name = $tmp[0];
            $method_name = $tmp[1];

            $getter_parsed = [
                'class_name' => $tmp[0],
                'method_name' => $tmp[1],
            ];
        }
        else
        {
            \Log::error('[Report::ReportManager::_check_dataset] getter value must be full classname::methode name',[
                'getter' => $getter
            ]);
        }

        return $getter_parsed;
    }
}

