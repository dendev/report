<?php

namespace Dendev\Report;

use Dendev\Report\Console\Commands\Install;
use Dendev\Report\Console\Commands\MakeReportCmd;
use Illuminate\Support\ServiceProvider;

class AddonServiceProvider extends ServiceProvider
{
    use AutomaticServiceProvider;

    protected $vendorName = 'dendev';
    protected $packageName = 'report';
    protected $commands = [Install::class, MakeReportCmd::class];
}
