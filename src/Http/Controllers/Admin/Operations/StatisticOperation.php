<?php

namespace Dendev\Report\Http\Controllers\Admin\Operations;

use Dendev\Report\Models\Report;
use Illuminate\Support\Facades\Route;

trait StatisticOperation
{
    /**
     * Define which routes are needed for this operation.
     *
     * @param string $segment    Name of the current entity (singular). Used as first URL segment.
     * @param string $routeName  Prefix of the route name.
     * @param string $controller Name of the current CrudController.
     */
    protected function setupStatisticRoutes($segment, $routeName, $controller)
    {
        Route::get($segment.'/statistic/{report_id}/{type?}', [
            'as'        => $routeName.'.statistic',
            'uses'      => $controller.'@statistic',
            'operation' => 'statistic',
        ]);
    }

    /**
     * Add the default settings, buttons, etc that this operation needs.
     */
    protected function setupStatisticDefaults()
    {
        $this->crud->allowAccess('statistic');

        $this->crud->operation('statistic', function () {
            $this->crud->loadDefaultOperationSettingsFromConfig();
        });

        $this->crud->operation('list', function () {
            $this->crud->addButton('line', 'statistic', 'view', 'dendev.report::buttons.statistic');
        });
        $this->crud->operation('show', function () {
            $this->crud->addButton('line', 'statistic', 'view', 'dendev.report::buttons.statistic');
        });
    }

    /**
     * Show the view for performing the operation.
     *
     * @return Response
     */
    public function statistic($report_id, $type = false)
    {
        $this->crud->hasAccessOrFail('statistic');

        $report = Report::find($report_id);
        $type = $type ? $type : $report->statistic_display;

        // prepare the fields you need to show
        $this->crud->entry = $report;
        $this->crud->entry = $report;
        $this->data['chart_json'] = \ReportManager::export_as_chart($type, $report, 'json');
        $this->data['statistic_displays'] = \ReportManager::get_available_statistic_displays();
        $this->data['statistic_display_current'] = $type;
        $this->data['crud'] = $this->crud;
        $this->data['title'] = $this->crud->getTitle() ?? 'statistic '.$this->crud->entity_name;

        // load the view
        return view("dendev.report::operations.statistic", $this->data);
    }
}
