<?php

namespace Dendev\Report\Http\Controllers\Admin\Operations;

use Illuminate\Support\Facades\Route;

trait RunOperation
{
    /**
     * Define which routes are needed for this operation.
     *
     * @param string $segment    Name of the current entity (singular). Used as first URL segment.
     * @param string $routeName  Prefix of the route name.
     * @param string $controller Name of the current CrudController.
     */
    protected function setupRunRoutes($segment, $routeName, $controller)
    {
        Route::get($segment.'/run/{report_id}', [
            'as'        => $routeName.'.run',
            'uses'      => $controller.'@run',
            'operation' => 'run',
        ]);
    }

    /**
     * Add the default settings, buttons, etc that this operation needs.
     */
    protected function setupRunDefaults()
    {
        $this->crud->allowAccess('run');

        $this->crud->operation('run', function () {
            $this->crud->loadDefaultOperationSettingsFromConfig();
        });

        $this->crud->operation('list', function () {
            $this->crud->addButton('line', 'run', 'view', 'dendev.report::buttons.run');
        });
        $this->crud->operation('show', function () {
            $this->crud->addButton('line', 'run', 'view', 'dendev.report::buttons.run');
        });
    }

    /**
     * Show the view for performing the operation.
     *
     * @return Response
     */
    public function run($report_id)
    {
        // check
        $this->crud->hasAccessOrFail('run');

        // action
        $values = \ReportManager::take_snapshot($report_id, false, 'string');

        // inform
        if( count($values) > 0)
        {
            \Alert::success(trans('dendev.report::report.operation_run_success'))->flash();
            $values_html = '<ul>';
            foreach( $values as $value)
                $values_html .= '<li>' . $value . '</li>';
            \Alert::info($values_html)->flash();
        }
        else
        {
            \Alert::warning(trans('dendev.report::report.operation_run_no_datas'))->flash();
        }

        // redirect
        $previous_url = url()->previous();
        return \Redirect::to($previous_url);
    }
}
