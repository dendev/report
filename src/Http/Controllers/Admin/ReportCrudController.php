<?php

namespace Dendev\Report\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
use Dendev\Report\Http\Controllers\Admin\Operations\RunOperation;
use Dendev\Report\Http\Controllers\Admin\Operations\StatisticOperation;
use Dendev\Report\Http\Requests\ReportRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class ReportCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ReportCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation { CreateOperation::store as traitStore; }
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation { UpdateOperation::update as traitUpdate; }
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use \Dendev\Report\Http\Controllers\Admin\Operations\RunOperation;
    use \Dendev\Report\Http\Controllers\Admin\Operations\StatisticOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\Dendev\Report\Models\Report::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/report');
        CRUD::setEntityNameStrings('report', 'reports');

        CRUD::allowAccess('run');
        CRUD::allowAccess('statistic');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::addColumn([
            'label' => ucfirst( trans('dendev.report::report.field_title') ),
            'name' => 'title',
            'type' => 'text'
        ]);

        CRUD::addColumn([
            'label' => ucfirst( trans('dendev.report::report.field_description') ),
            'name' => 'description',
            'type' => 'text'
        ]);
    }

    protected function setupShowOperation()
    {
        CRUD::addColumn([
            'label' => ucfirst( trans('dendev.report::report.field_title') ),
            'name' => 'title',
            'type' => 'text'
        ]);

        CRUD::addColumn([
            'label' => ucfirst( trans('dendev.report::report.field_slug') ),
            'name' => 'slug',
            'type' => 'text'
        ]);

        CRUD::addColumn([
            'label' => ucfirst( trans('dendev.report::report.field_description') ),
            'name' => 'description',
            'type' => 'text'
        ]);

        $statistic_diplays = \ReportManager::get_available_statistic_displays();
        CRUD::addColumn([
            'label'       => ucfirst( trans('dendev.report::report.field_statistic_display') ),
            'name'        => 'statistic_display',
            'type'        => 'select2_from_array',
            'options'     => $statistic_diplays,
            'allows_null' => false,
            'default'     => 'line',
        ]);

        CRUD::addColumn([
            'label' => ucfirst( trans('dendev.report::report.field_labels') ),
            'name' => 'labels_bkp',
            'type' => 'table',
            'columns' => ['label' => trans('dendev.report::report.field_label') ]
        ]);

        CRUD::addColumn([
            'label' => ucfirst( trans('dendev.report::report.field_datasets') ),
            'name' => 'datasets',
            'type' => 'view',
            'view'  => 'dendev.report::columns.datasets',
        ]);

        CRUD::addColumn([
            'label' => ucfirst( trans('dendev.report::report.field_config') ),
            'name' => 'config',
            'type' => 'view',
            'view'  => 'dendev.report::columns.config',
        ]);
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(ReportRequest::class);

        CRUD::addField([
            'label' => ucfirst( trans('dendev.report::report.field_title') ),
            'name' => 'title',
            'type' => 'text',
        ]);

        CRUD::addField([
            'label' => ucfirst( trans('dendev.report::report.field_description') ),
            'name' => 'description',
            'type' => 'textarea'
        ]);

        $statistic_diplays = \ReportManager::get_available_statistic_displays();
        CRUD::addField([
            'label'       => ucfirst( trans('dendev.report::report.field_statistic_display') ),
            'name'        => 'statistic_display',
            'type'        => 'select2_from_array',
            'options'     => $statistic_diplays,
            'allows_null' => false,
            'default'     => 'line',
        ]);

        CRUD::addField([
            'label' => ucfirst( trans('dendev.report::report.field_labels') ),
            'name' => 'labels_bkp',
            'type' => 'repeatable',
            'fields' => [
                [
                    'name'    => 'label',
                    'type'    => 'text',
                    'label' => ucfirst( trans('dendev.report::report.field_label') ),
                    'wrapper' => ['class' => 'form-group col-md-12'],
                ],
            ],

            // optional
            'new_item_label'  => ucfirst( trans('dendev.report::report.field_label_add') ),
        ]);

        CRUD::addField([
            'label' => ucfirst( trans('dendev.report::report.field_datasets') ),
            'name' => 'datasets',
            'type' => 'repeatable',
            'fields' => [
                [
                    'name'    => 'label',
                    'type'    => 'text',
                    'label' => ucfirst( trans('dendev.report::report.field_dataset_label') ),
                    'wrapper' => ['class' => 'form-group col-md-6'],
                ],
                [
                    'name'    => 'getter',
                    'type'    => 'text',
                    'label' => ucfirst( trans('dendev.report::report.field_dataset_getter') ),
                    'wrapper' => ['class' => 'form-group col-md-6'],
                ],
                [
                    'name'    => 'data',
                    'type'    => 'text',
                    'label' => ucfirst( trans('dendev.report::report.field_dataset_data') ),
                    'wrapper' => ['class' => 'form-group col-md-12'],
                ],
                [
                    'name'    => 'border_color',
                    'type'    => 'text',
                    'label' => ucfirst( trans('dendev.report::report.field_dataset_border_color') ),
                    'hint' => ucfirst( trans('dendev.report::report.field_dataset_border_color_hint') ),
                    'wrapper' => ['class' => 'form-group col-md-6'],
                ],
                [
                    'name'    => 'background_color',
                    'type'    => 'text',
                    'label' => ucfirst( trans('dendev.report::report.field_dataset_background_color') ),
                    'hint' => ucfirst( trans('dendev.report::report.field_dataset_background_color_hint') ),
                    'wrapper' => ['class' => 'form-group col-md-6'],
                ],
            ],
            // optional
            'new_item_label'  => ucfirst( trans('dendev.report::report.field_dataset_add') ),
        ]);

        CRUD::addField([
            'name'     => 'config', // JSON variable name
            'label'    => ucfirst( trans('dendev.report::report.field_config') ),
            'type'     => 'config',
            'view_namespace'  => 'dendev.report::fields',
        ]);
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    public function store()
    {
        $this->crud->hasAccessOrFail('create');
        return $this->_store_and_upate();
    }

    public function update()
    {
        $this->crud->hasAccessOrFail('update');
        return $this->_store_and_upate($is_udpate = true);
    }

    private function _store_and_upate($is_update = false)
    {
        // execute the FormRequest authorization and validation, if one is required
        $request = $this->crud->validateRequest();

        // get datas
        $datas =  $request->all();

        $title = $datas['title'];
        $description = ( $datas['description'] ) ? $datas['description'] : false;
        $statistic_display = $datas['statistic_display'];
        $labels = $datas['labels_bkp'];
        $datasets = $datas['datasets'];

        // config
        $config = [];
        if( $datas['checkbox_config_responsive'] )
            $config['responsive'] = $datas['checkbox_config_responsive'];
        if( $datas['select_config_legend_position'] )
            $config['legend']['position'] = $datas['select_config_legend_position'];

        // format datas
        //   //  labels
        $tmp_labels = json_decode($labels, true);
        if(is_array($labels));
        $labels_formated = [];
        foreach( $tmp_labels as $tmp_label)
        {
            $labels_formated[] = $tmp_label['label'];
        }

        //   // datasets
        $datasets = json_decode($datasets, true);
        $datasets_formated = [];
        foreach( $datasets as $key => $dataset )
        {
            //   //  //  dataset
            $dataset['data'] = explode(',', $dataset['data']);
            $datasets_formated[$key] = $dataset;
        }

        $config = ( count($config) > 0 ) ? $config : false;

        // create
        $report = \ReportManager::create($title, $labels_formated, $datasets_formated, $config, $description, $statistic_display);

        // for backpack
        $this->data['entry'] = $this->crud->entry = $report; // utils ??

        // inform
        if( $report )
            \Alert::success(trans('backpack::crud.insert_success'))->flash();
        else
            \Alert::error(trans('dendev.report::report.insert_error'))->flash();

        // save the redirect choice for next time
        $this->crud->setSaveAction();

        return $this->crud->performSaveAction($report->id);
    }
}
