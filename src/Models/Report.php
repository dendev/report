<?php

namespace Dendev\Report\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Report extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'reports';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */
    public function getLabelsAttribute($value)
    {
        if( $value )
            $value = json_decode($value, true);

        return $value;
    }

    public function getLabelsBkpAttribute($value) // sepcialy for backpack crud column
    {
        $labels_bkp = null;

        $labels = $this->attributes['labels'];
        if( $labels )
        {
            $labels = json_decode($labels, true);
            foreach( $labels as $label )
            {
                $labels_bkp[] = ['label' => $label];
            }

        }

        return $labels_bkp;
    }

    public function getDatasetsAttribute($value)
    {
        if( $value )
            $value = json_decode($value, true);

        return $value;
    }

    public function getConfigAttribute($value)
    {
        if( $value )
            $value = json_decode($value, true);

        return $value;
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
    public function setTitleAttribute($value)
    {
        $this->attributes['title'] = ucfirst($value);
        $this->attributes['slug'] = \Str::slug($value);
    }

    public function setLabelsAttribute($value)
    {
        if( is_array($value))
            $this->attributes['labels'] = json_encode($value);
        else
            $this->attributes['labels'] = $value;
    }

    public function setDatasetsAttribute($value)
    {
        if( is_array($value))
            $this->attributes['datasets'] = json_encode($value);
        else
            $this->attributes['datasets'] = $value;
    }

    public function setConfigAttribute($value)
    {
        if( is_array($value))
            $this->attributes['config'] = json_encode($value);
        else
            $this->attributes['config'] = $value;
    }


    /*
    |--------------------------------------------------------------------------
    | Events
    |--------------------------------------------------------------------------
    */
    /* // need laravel 7
    protected static function booted()
    {
        static::saving(function ($report) {
            $report->slug = Str::slug($report->title);
        });
    }
    */
}
