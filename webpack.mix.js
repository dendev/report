const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Backpack maintainers use mix to:
 | - install and update CSS and JS assets;
 | - copy everything that needs to be published into src/public
 |
 | All JS will be bundled into one file (see bundle.js).
 |
 | How to use (for maintainers only):
 | - cd vendor/dendev/report
 | - npm install
 | - npm run prod
 | (this will also publish the assets for you to test, so no need to do that too)
 */

// merge all needed JS into a big bundle file
mix.js('resources/assets/js/chart.js', 'public/packages/dendev/report/js/');

// merge all needed CSS into a big bundle file
mix.sass('resources/assets/scss/statistic.scss', 'public/packages/dendev/report/css/')
	.options({
      processCssUrls: false
    });
// copy CRUD filters JS into packages
mix.copy('node_modules/chart.js/dist', 'public/packages/chart.js/dist')

// FOR MAINTAINERS
// copy asset files from Base's public folder the main app's public folder
// so that you don't have to publish the assets with artisan to test them
mix.copyDirectory('public', '../../../public')
