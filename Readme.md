# Report

> Rapport de données et affichage sous forme de graphique 

Permet la création d'un rapport qui va "surveiller" des données et permettre ensuite leur affichage en graphique

## Install
```bash
composer config repositories.report vcs https://gitlab.com/dendev/report.git
composer require dendev/report

# avec publication des fichiers langs et config
php artisan report:install --publish

# sans publication des fichiers langs et config
php artisan report:install

# publication des assets dans public/packages
php artisan vendor:publish --provider="Dendev\Report\AddonServiceProvider" --tag=public_assets
```

## Config

Pas de configuration à ajouter.

## Lang

```bash
 php artisan vendor:publish --provider="Dendev\Report\AddonServiceProvider" --tag=lang
```

## Usage

Crée un methode qui va venir récupérer une donnée et la retourner. 

Sur la parti admin dans la page report, crée un nouvel report.  
Un report est fait de n source de données celles ci sont alimentés par des méthodes dont l'implantation dépend du developpeur.  

La récupération des données se fait via l'ajout d'une commande laraval dans un cron.
