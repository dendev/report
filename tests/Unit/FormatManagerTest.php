<?php

namespace Tests\Unit;

use Dendev\Report\Models\Report;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Str;
use Orchestra\Testbench\TestCase;
use Illuminate\Auth\SessionGuard;
use Tests\Unit\Mocks\Getter;

class FormatManagerTest extends TestCase
{
    use RefreshDatabase;


    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
    }

    protected function getPackageProviders($app)
    {
        return [
            'Dendev\Report\AddonServiceProvider',
            'Backpack\CRUD\BackpackServiceProvider',
        ];
    }

    protected function getEnvironmentSetUp($app)
    {
        $config = include './tests/config.php';
        $mysql_connection = $config['db']['mysql'];

        $app['config']->set('database.default', 'mysql');
        $app['config']->set('database.connections.mysql', $mysql_connection);
        $app['config']->set('auth.providers.users.model', 'App\Models\User');
    }

    //
    public function testBasic()
    {
        $exist = \FormatManager::test_me();
        $this->assertTrue($exist);
    }

    public function testExportAsChartLine()
    {
        $title = 'test 1';
        $labels = ["one", "two", "three"];

        $data_1 = [1,2,3,4,5];
        $data_2 = [5,6,8,9,15];

        $datasets = [
            ['label' => 'dt 1', 'data' => $data_1, 'getter' => 'Tests\Unit\Mocks\Getter::get_1'],
            ['label' => 'dt 2', 'data' => $data_2, 'getter' => 'Tests\Unit\Mocks\Getter::get_2'],
        ];
        $report = \ReportManager::create($title, $labels, $datasets);

        $datas = \ReportManager::export_as_chart('line', $report, 'raw');

        $this->assertArrayHasKey('type', $datas);
        $this->assertArrayHasKey('data', $datas);

        $this->assertArrayHasKey('labels', $datas['data']);
        $this->assertArrayHasKey('datasets', $datas['data']);

        $this->assertArrayHasKey('label', $datas['data']['datasets'][0]);
        $this->assertArrayHasKey('data', $datas['data']['datasets'][0]);
        $this->assertArrayHasKey('fill', $datas['data']['datasets'][0]);
        $this->assertArrayHasKey('borderColor', $datas['data']['datasets'][0]);
        $this->assertArrayHasKey('tension', $datas['data']['datasets'][0]);
    }
}
