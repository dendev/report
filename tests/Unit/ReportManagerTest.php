<?php

namespace Tests\Unit;

use Dendev\Report\Models\Report;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Str;
use Orchestra\Testbench\TestCase;
use Illuminate\Auth\SessionGuard;
use Tests\Unit\Mocks\Getter;

class ReportManagerTest extends TestCase
{
    use RefreshDatabase;


    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
    }

    protected function getPackageProviders($app)
    {
        return [
            'Dendev\Report\AddonServiceProvider',
            'Backpack\CRUD\BackpackServiceProvider',
        ];
    }

    protected function getEnvironmentSetUp($app)
    {
        $config = include './tests/config.php';
        $mysql_connection = $config['db']['mysql'];

        $app['config']->set('database.default', 'mysql');
        $app['config']->set('database.connections.mysql', $mysql_connection);
        $app['config']->set('auth.providers.users.model', 'App\Models\User');
    }

    //
    public function testBasic()
    {
        $exist = \ReportManager::test_me();
        $this->assertTrue($exist);
    }

    public function testCreate()
    {
        $title = 'test 1';
        $labels = ["one", "two", "three"];
        $datasets = [
            ['label' => 'dt 1', 'data' => [1,2,3,4,5], 'getter' => 'test'],
            ['label' => 'dt 2', 'data' => [5,6,8,9,15], 'getter' => 'test 2'],
        ];
        $report = \ReportManager::create($title, $labels, $datasets);

        $this->assertEquals($report->title, ucfirst($title));
        $this->assertEquals($report->slug, Str::slug($title));
    }

    public function testCreateAlreadyExist()
    {
        $title_1 = 'test 1';
        $labels = ["one", "two", "three"];
        $datasets = [
            ['label' => 'dt 1', 'data' => [1,2,3,4,5], 'getter' => 'test'],
            ['label' => 'dt 2', 'data' => [5,6,8,9,15], 'getter' => 'test 2'],
        ];
        $description_1 = 'desc 1';
        $description_2 = 'desc 2';

        $report_1 = \ReportManager::create($title_1, $labels, $datasets, false, $description_1);
        $report_2 = \ReportManager::create($title_1, $labels, $datasets, false, $description_2);

        $report_1->refresh();

        $this->assertEquals($report_1->title, $report_2->title);
        $this->assertEquals($report_1->slug, $report_2->slug);
        $this->assertEquals($report_1->description, $report_2->description);
    }

    public function testAddDatasetToReport()
    {
        $title = 'test 1';
        $labels = ["one", "two", "three"];
        $datasets = [
            ['label' => 'dt 1', 'data' => [1,2,3,4,5], 'getter' => 'test'],
        ];
        $dataset = ['label' => 'dt 2', 'data' => [5,6,8,9,15], 'getter' => 'test 2'];
        $description = 'desc 1';

        $report = \ReportManager::create($title, $labels, $datasets, false, $description);

        $datasets = $report->datasets;
        $this->assertCount(1, $datasets);

        $report = \ReportManager::add_dataset_to_report( $dataset, $report);
        $datasets = $report->datasets;
        $this->assertCount(2, $datasets);
    }

    public function testGetDataFromDatasetOfReport()
    {
        $title = 'test 1';
        $labels = ["one", "two", "three"];

        $data = [1,2,3,4,5];
        $datasets = [
            ['label' => 'dt 1', 'data' => $data, 'getter' => 'test'],
        ];

        $report = \ReportManager::create($title, $labels, $datasets);
        $report_data = \ReportManager::get_data_from_dataset_of_report(0, $report);

        $this->assertEquals($data, $report_data);
    }

    public function testAddDataToDatasetOfReport()
    {
        $title = 'test 1';
        $labels = ["one", "two", "three"];

        $data = [1,2,3,4,5];
        $data_udpated = [1,2,3,4,5, 6, 7, 8];
        $datasets = [
            ['label' => 'dt 1', 'data' => $data, 'getter' => 'test'],
        ];

        // create
        $report = \ReportManager::create($title, $labels, $datasets);
        $this->assertEquals(\ReportManager::get_data_from_dataset_of_report(0, $report), $data);

        // add
        $report = \ReportManager::add_data_to_dataset_of_report([6,7,8,], 0, $report);
        $this->assertEquals(\ReportManager::get_data_from_dataset_of_report(0, $report), $data_udpated);
    }

    public function testGetGetterFromDatasetOfReport()
    {
        $title = 'test 1';
        $labels = ["one", "two", "three"];
        $datasets = [
            ['label' => 'dt 1', 'data' => [1,2,3,4,5], 'getter' => 'Tests\Unit\Mocks\Getter::get_1'],
            ['label' => 'dt 2', 'data' => [5,6,8,9,15], 'getter' => 'Tests\Unit\Mocks\Getter::get_2'],
        ];
        $report = \ReportManager::create($title, $labels, $datasets);

        $getter_0 = \ReportManager::get_getter_from_dataset_of_report(0, $report);
        $getter_1 = \ReportManager::get_getter_from_dataset_of_report(1, $report);

        $this->assertEquals('Tests\Unit\Mocks\Getter::get_1', $getter_0);
        $this->assertEquals('Tests\Unit\Mocks\Getter::get_2', $getter_1);
    }

    public function testResetDataFromDatasetOfReport()
    {
        $title = 'test 1';
        $labels = ["one", "two", "three"];

        $data_1 = [1,2,3,4,5];
        $data_2 = [5,6,8,9,15];
        $datasets = [
            ['label' => 'dt 1', 'data' => $data_1, 'getter' => 'Tests\Unit\Mocks\Getter::get_1'],
            ['label' => 'dt 2', 'data' => $data_2, 'getter' => 'Tests\Unit\Mocks\Getter::get_2'],
        ];
        $report = \ReportManager::create($title, $labels, $datasets);

        \ReportManager::reset_data_from_dataset_of_report(0, $report);

        $report_data_1 = \ReportManager::get_data_from_dataset_of_report(0, $report);
        $report_data_2 = \ReportManager::get_data_from_dataset_of_report(1, $report);

        $this->assertEquals($data_1, $report_data_1);
        $this->assertEquals($data_2, $report_data_2);
    }

    public function testRunGetterFromDatasetOfReport()
    {
        $title = 'test 1';
        $labels = ["one", "two", "three"];
        $datasets = [
            ['label' => 'dt 1', 'data' => [1,2,3,4,5], 'getter' => 'Tests\Unit\Mocks\Getter::get_1'],
            ['label' => 'dt 2', 'data' => [5,6,8,9,15], 'getter' => 'Tests\Unit\Mocks\Getter::get_2'],
        ];
        $report = \ReportManager::create($title, $labels, $datasets);

        $data = \ReportManager::run_getter_from_dataset_of_report(0, $report);

        $getter = new Getter();
        $data_getter = $getter->get_1();

        $this->assertEquals($data_getter, $data);
    }

    public function testTakeSnapshot()
    {
        $title = 'test 1';
        $labels = ["one", "two", "three"];

        $data_1 = [1,2,3,4,5];
        $data_2 = [5,6,8,9,15];

        $snap_data_1 = 'a';
        $snap_data_2 = 99;

        $datasets = [
            ['label' => 'dt 1', 'data' => $data_1, 'getter' => 'Tests\Unit\Mocks\Getter::get_1'],
            ['label' => 'dt 2', 'data' => $data_2, 'getter' => 'Tests\Unit\Mocks\Getter::get_2'],
        ];
        $report = \ReportManager::create($title, $labels, $datasets);

        $datasets = \ReportManager::take_snapshot($report);

        $this->assertCount(2, $datasets);
        $this->assertEquals($datasets[0]['data'], $snap_data_1);
        $this->assertEquals($datasets[1]['data'], $snap_data_2);
    }

    public function testTakeSnapshotWithSave()
    {
        $title = 'test 1';
        $labels = ["one", "two", "three"];

        $data_1 = [1,2,3,4,5];
        $data_2 = [5,6,8,9,15];

        $datasets = [
            ['label' => 'dt 1', 'data' => $data_1, 'getter' => 'Tests\Unit\Mocks\Getter::get_1'],
            ['label' => 'dt 2', 'data' => $data_2, 'getter' => 'Tests\Unit\Mocks\Getter::get_2'],
        ];
        $report = \ReportManager::create($title, $labels, $datasets);
        $datasets = \ReportManager::take_snapshot($report, true);

        $report->refresh();

        $this->assertCount(2, $datasets);
        $this->assertEquals($datasets[0]['data'], last($report->datasets[0]['data']));
        $this->assertEquals($datasets[1]['data'], last($report->datasets[1]['data']));
    }

    public function testExportAsChart()
    {
        $title = 'test 1';
        $labels = ["one", "two", "three"];

        $data_1 = [1,2,3,4,5];
        $data_2 = [5,6,8,9,15];

        $datasets = [
            ['label' => 'dt 1', 'data' => $data_1, 'getter' => 'Tests\Unit\Mocks\Getter::get_1'],
            ['label' => 'dt 2', 'data' => $data_2, 'getter' => 'Tests\Unit\Mocks\Getter::get_2'],
        ];
        $report = \ReportManager::create($title, $labels, $datasets);

        $datas = \ReportManager::export_as_chart('line', $report, 'raw');

        $this->assertArrayHasKey('type', $datas);
        $this->assertArrayHasKey('data', $datas);

        $this->assertArrayHasKey('labels', $datas['data']);
        $this->assertArrayHasKey('datasets', $datas['data']);

        $this->assertArrayHasKey('label', $datas['data']['datasets'][0]);
        $this->assertArrayHasKey('data', $datas['data']['datasets'][0]);
        $this->assertArrayHasKey('fill', $datas['data']['datasets'][0]);
        $this->assertArrayHasKey('borderColor', $datas['data']['datasets'][0]);
        $this->assertArrayHasKey('tension', $datas['data']['datasets'][0]);
    }
}
