<?php

namespace App\Console\Commands;

use Dendev\Report\Models\Report;
use Illuminate\Console\Command;

class ReportDemandCreatedByMonth extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'report:report_demand_created_by_months {--fake : Simulation ne modifiant pas la db}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $ok = 0;
        $report_slug = 'report_demand_created_by_months';
        $is_fake =  $this->option('fake');

        $report = Report::where('slug', $report_slug)->first();
        if( $report)
        {
            $save = $is_fake ? false : true;
            $this->info("[Report] Snapshot datas of '$report_slug'" );
            \ReportManager::take_snapshot( $report, $save);
        }
        else
        {
            $this->info("[Report] Report : '$report_slug' not found");
            \Log::error("[Report::Cmd] no report found", [
                'report' => $report
            ]);

            $ok = 127;
        }

        if( $ok === 0 )
            $this->info("[Report] Ok" );
        else
            $this->error("[Report] Ko" );


        return $ok;
    }
}
